﻿using LessonOTUS32.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS32
{
    class Program
    {
        static void Main(string[] args)
        {
            Storage storage = new Storage();

            Dog dog = new Dog("Dog", 12);
            dog.FeedDog();
            Animal animal = dog;
            Models model = new Models(animal);
            model.ActAnimal();
            storage.Save(animal);

            Cat cat = new Cat("Cat", 22);
            cat.FeedCat();
            animal = cat;
            model = new Models(animal);
            model.ActAnimal();
            storage.Save(animal);
        }
    }
}
