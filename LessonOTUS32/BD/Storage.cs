﻿using System;

namespace LessonOTUS32
{
    public class Storage
    {
        MyConsole myConsole = new MyConsole();

        public void Save(Animal animal)
        {
            myConsole.ShowLine("Animal saved: " + animal.Name);
        }

        public IAnimal GetAnimal(string animalName)
        {
            myConsole.ShowLine("Animal received");
            return new Dog(animalName, 14);
        }
    }
}
