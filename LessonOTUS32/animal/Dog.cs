﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS32
{
    public class Dog : Animal, IDog
    {
        MyConsole myConsole = new MyConsole();

        public Dog(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public override void Pronounce()
        {
            myConsole.ShowLine("Wow Wow!");
        }

        public override void CountPalms()
        {
            myConsole.ShowLine("Palms: 6");
        }

        public void FeedDog()
        {
            myConsole.ShowLine("Cat fed");
        }
    }
}
