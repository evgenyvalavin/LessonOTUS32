﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS32
{
    public interface IAnimal
    {
        void Pronounce();
        void CountPalms();
    }
}