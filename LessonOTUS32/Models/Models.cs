﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS32.Model
{
    public class Models
    {
        IAnimal animal;

        public Models(IAnimal animal)
        {
            this.animal = animal;
        }

        public void ActAnimal()
        {
            animal.CountPalms();
            animal.Pronounce();
        }
    }
}
