﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS32
{
    public class Cat : Animal, ICat
    {
        MyConsole myConsole = new MyConsole();

        public Cat(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public override void CountPalms()
        {
            myConsole.ShowLine("Palms: 59");
        }

        public void FeedCat()
        {
            myConsole.ShowLine("Cat fed");
        }

        public override void Pronounce()
        {
            myConsole.ShowLine("Myay");
        }
    }
}
